package ch.ribeaud.idorsia

import com.actelion.research.orbit.imageAnalysis.components.RecognitionFrame
import com.actelion.research.orbit.imageAnalysis.models.OrbitModel
import com.actelion.research.orbit.imageAnalysis.models.SegmentationResult
import com.actelion.research.orbit.imageAnalysis.utils.OrbitHelper
import com.actelion.research.orbit.imageAnalysis.utils.OrbitLogAppender
import com.actelion.research.orbit.imageAnalysis.utils.OrbitUtils
import spock.lang.Specification

/**
 * Test cases on object segmentation
 *
 * @author Christian Ribeaud
 */
class ObjectSegmentationSpecification extends Specification {

    private OrbitModel orbitModel
    private OrbitModel MfsOrbitModel

    def setup() {
        OrbitLogAppender.GUI_APPENDER = false
        OrbitUtils.SLEEP_TASK=0
        OrbitUtils.SLEEP_TILE=0
        orbitModel = OrbitModel.LoadFromInputStream(this.getClass().getResourceAsStream("/segmentation.omo"))
        MfsOrbitModel = OrbitModel.LoadFromInputStream(this.getClass().getResourceAsStream("/segmentation-mfs.omo"))
    }

    void 'Should return correct number of spots found for "segmentation.jpg"'() {
        given:
        RecognitionFrame recognitionFrame = new RecognitionFrame("/segmentation.jpg", true)
        when:
        SegmentationResult segmentationResult = OrbitHelper.Segmentation(recognitionFrame, -1, orbitModel, null, -1, false)
        SegmentationResult mfSSegmentationResult = OrbitHelper.Segmentation(recognitionFrame, -1, MfsOrbitModel, null, -1, false)
        then:
        segmentationResult
        segmentationResult.objectCount == 2295
        // Do I get similar results with 'Mumford-Shah'?
        mfSSegmentationResult
        mfSSegmentationResult.objectCount == 1334
    }

    void 'Should return correct number of spots found for "mfs-segmentation.jpg"'() {
        given:
        RecognitionFrame recognitionFrame = new RecognitionFrame("/mfs-segmentation.jpg", true)
        when:
        SegmentationResult segmentationResult = OrbitHelper.Segmentation(recognitionFrame, -1, orbitModel, null, -1, false)
        SegmentationResult mfSSegmentationResult = OrbitHelper.Segmentation(recognitionFrame, -1, MfsOrbitModel, null, -1, false)
        then:
        segmentationResult
        // Two level segmentation does NOT to work on that case...
        segmentationResult.objectCount == 0
        mfSSegmentationResult
        mfSSegmentationResult.objectCount == 94
    }

    void 'Should return correct number of spots found for "Week1_150607_B03_s1_w2CAEC09A2-969B-48A9-BE0B-C6825D180EB4-rgb.png"'() {
        given:
        RecognitionFrame recognitionFrame = new RecognitionFrame("/Week1_150607_B03_s1_w2CAEC09A2-969B-48A9-BE0B-C6825D180EB4.rgb.png", true)
        when:
        SegmentationResult segmentationResult = OrbitHelper.Segmentation(recognitionFrame, -1, orbitModel, null, -1, false)
        SegmentationResult mfSSegmentationResult = OrbitHelper.Segmentation(recognitionFrame, -1, MfsOrbitModel, null, -1, false)
        then:
        segmentationResult
        segmentationResult.objectCount == 27
        mfSSegmentationResult
        // 'Mumford-Shah' does NOT work on that case...
        mfSSegmentationResult.objectCount == 0
    }

    // Converted to 24-bit using 'ImageUtils'
    void 'Should return correct number of spots found for "Week1_150607_E05_s1_w25638D64A-4A43-43C2-A060-5136F0676AE6.png"'() {
        given:
        RecognitionFrame recognitionFrame = new RecognitionFrame("/Week1_150607_E05_s1_w25638D64A-4A43-43C2-A060-5136F0676AE6.png", true)
        when:
        SegmentationResult segmentationResult = OrbitHelper.Segmentation(recognitionFrame, -1, orbitModel, null, -1, false)
        SegmentationResult mfSSegmentationResult = OrbitHelper.Segmentation(recognitionFrame, -1, MfsOrbitModel, null, -1, false)
        then:
        segmentationResult
        segmentationResult.objectCount == 86
        mfSSegmentationResult
        mfSSegmentationResult.objectCount == 453
    }

}

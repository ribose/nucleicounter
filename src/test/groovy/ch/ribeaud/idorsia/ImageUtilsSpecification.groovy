package ch.ribeaud.idorsia

import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Shared
import spock.lang.Specification

import javax.imageio.ImageIO
import java.awt.image.BufferedImage
import java.nio.file.Files
import java.nio.file.Paths

/**
 * Test cases for corresponding class {@link ImageUtils}.
 *
 * @author Christian Ribeaud
 */
class ImageUtilsSpecification extends Specification {

    @Rule
    TemporaryFolder tempFolder
    @Shared
    File targetFile

    def "Should have converted the original image to RGB one"() {
        given:
        def source = Paths.get(ImageUtilsSpecification.class.getResource('/Week1_150607_B03_s1_w2CAEC09A2-969B-48A9-BE0B-C6825D180EB4.orig.tif').toURI())
        assert source
        assert Files.exists(source)
        def target = tempFolder.newFolder('rgb').toPath().resolve('converted.jpg')
        assert target
        Files.copy(source, target)
        assert Files.exists(target)
        targetFile = target.toFile()
        when:
        BufferedImage bufferedImage = ImageIO.read(target.toFile())
        assert bufferedImage
        ImageIO.write(ImageUtils.convertToRgbImage(bufferedImage), 'png', targetFile)
        then:
        targetFile.exists()
        targetFile.length() == 494345
    }

    def "Should have removed the file by now"() {
        expect:
        !targetFile.exists()
    }

}
package ch.ribeaud.idorsia

import ij.io.Opener
import spock.lang.Specification

/**
 * Test cases around <b>ImageJ</b>.
 *
 * @author Christian Ribeaud
 */
class ImageJSpecification extends Specification {

    void 'Should load TIF image file'() {
        given:
        Opener opener = new Opener()
        def stream = ImageJSpecification.class.getResourceAsStream("/Week1_150607_B03_s1_w2CAEC09A2-969B-48A9-BE0B-C6825D180EB4.orig.tif")
        assert stream != null
        when:
        def imagePlus = opener.openTiff(stream, "TIF file")
        then:
        imagePlus
        imagePlus.bitDepth == 16
    }

    void 'Should load PNG image file an return correct number of bit depth'() {
        given:
        Opener opener = new Opener()
        def url = ImageJSpecification.class.getResource("/Week1_150607_B03_s1_w2CAEC09A2-969B-48A9-BE0B-C6825D180EB4.rgb.png")
        assert url != null
        when:
        def imagePlus = opener.openURL(url.toExternalForm())
        then:
        imagePlus
        imagePlus.bitDepth == 24
    }
}
package ch.ribeaud.idorsia;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Utility methods around images.
 *
 * @author <a href="mailto:christian@ribeaud.ch">Christian Ribeaud</a>
 */
public final class ImageUtils {

    private ImageUtils() {
        // Can NOT be instantiated
    }

    /**
     * Converts given <i>image</i> into image of type <code>BufferedImage.TYPE_INT_RGB</code>.
     *
     * @param image the image to convert. Can NOT be <code>null</code>.
     * @return the converted image. Never <code>null</code>.
     */
    public static Image convertToRgbImage(Image image) {
        assert image != null : "Unspecified image";
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        BufferedImage newImage = new BufferedImage(
                width, height, BufferedImage.TYPE_INT_RGB);
        Graphics g = newImage.getGraphics();
        g.drawImage(image, 0, 0, null);
        g.dispose();
        return newImage;
    }
}

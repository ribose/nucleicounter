package ch.ribeaud.idorsia

import com.actelion.research.orbit.imageAnalysis.components.RecognitionFrame
import com.actelion.research.orbit.imageAnalysis.models.OrbitModel
import com.actelion.research.orbit.imageAnalysis.utils.OrbitHelper
import groovyx.gpars.GParsPool
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVPrinter
import org.apache.commons.io.FileUtils
import org.apache.commons.io.FilenameUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.imageio.ImageIO
import java.awt.image.BufferedImage

/**
 * Main class.
 *
 * @author Christian Ribeaud
 */
class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class)
    // With the hope that the returned 'OrbitModel' is thread-safe... :s
    private static final OrbitModel orbitModel = OrbitModel
            .LoadFromInputStream(this.getClass().getResourceAsStream("/segmentation.omo"))

    static def handleFile(File targetDir, File file) {
        logger.info("Handling following file '{}'", file.absolutePath)
        BufferedImage bufferedImage = ImageIO.read(file)
        def png = new File(targetDir, FilenameUtils.getBaseName(file.getName()) + '.png')
        // Write out PNG file
        ImageIO.write(ImageUtils.convertToRgbImage(bufferedImage), 'png', png)
        RecognitionFrame recognitionFrame = new RecognitionFrame(png, true)
        OrbitHelper.Segmentation(recognitionFrame, -1, orbitModel, null, -1, false)
    }

    //
    // Main method
    //

    static void main(String[] args) {
        // Starting directory
        File workingDir = new File('/Users/christianr/Downloads/Idorsia/Week1_22123')
        // Where RGBs will be stored
        File rgbDir = new File(workingDir.parentFile, "rgbs")
        if (!rgbDir.exists()) {
            rgbDir.mkdir()
        } else {
            FileUtils.cleanDirectory(rgbDir)
        }
        // Process the files in parallel
        GParsPool.withPool {
            def segmentationResultByFile = workingDir.listFiles().collectParallel {
                [it, handleFile(rgbDir, it)]
            }.collectEntries { [(it[0]): it[1]] }
            // Write out the results to a CSV file
            new File("results.txt").withWriter { fileWriter ->
                def csvFilePrinter = new CSVPrinter(fileWriter, CSVFormat.DEFAULT)
                csvFilePrinter.printRecord(['FileName', 'Count'])
                segmentationResultByFile.each { key, value ->
                    csvFilePrinter.printRecord([key.name, value.objectCount])
                }
            }
        }
        println "Finished!"
    }
}

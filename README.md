# Scientific Computing Programming Challenge

## Application challenge [OPTIONAL]

If you want to apply, please demonstrate your skills by solving the following task.

1. Download the cell images from [here][1].
1. Write a Java program which outputs the nuclei count per fields of view. It should build with Gradle or similar. You can use any existing frameworks or write the algorithm on your own.
1. It can be a pragmatic solution, not unit-tests etc. needed.
1. Please send the solution + results as csv via email.
1. If you are invited you will present and defend your solution.


[1]: https://data.broadinstitute.org/bbbc/BBBC021/BBBC021_v1_images_Week1_22123.zip